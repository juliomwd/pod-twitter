package modelo;

import java.io.Serializable;
//Classe que define o tipo de solicitação enviada e os dados necessários para a determinada solicitação
//Serializable pois será um objeto enviado pela rede
public class Solicitacao implements Serializable {
	
	//Identificador do tipo de solicitação
	private TipoSolicitacao tipo;
	//Mensagem, caso a solicitação envie uma
	private Mensagem mensagem;
	//Seguidor que emitiu a solicitação
	private Seguidor seguidor;
	private static final long serialVersionUID = 1L;

	public Solicitacao(TipoSolicitacao tipo, Seguidor seguidor,Mensagem mensagem) {
		this.tipo = tipo;
		this.setSeguidor(seguidor);
		this.mensagem = mensagem;
	}
	
	public Solicitacao(TipoSolicitacao tipo, Seguidor seguidor){
		this.tipo = tipo;
		this.seguidor = seguidor;
		this.mensagem = null;
	}
	
	public Solicitacao(TipoSolicitacao tipo, Mensagem mensagem){
		this.tipo = tipo;
		this.mensagem = mensagem;
		this.seguidor = null;
	}
	

	public TipoSolicitacao getTipo() {
		return tipo;
	}

	public void setTipo(TipoSolicitacao tipo) {
		this.tipo = tipo;
	}

	public Mensagem getMensagem() {
		return mensagem;
	}

	public void setMensagem(Mensagem mensagem) {
		this.mensagem = mensagem;
	}

	public Seguidor getSeguidor() {
		return seguidor;
	}

	public void setSeguidor(Seguidor seguidor) {
		this.seguidor = seguidor;
	}

	
}
