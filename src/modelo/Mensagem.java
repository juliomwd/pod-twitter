package modelo;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

//Classe que encapsula o texto enviado e a respectiva data de envio
public class Mensagem implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String emissor;
	private String texto;
	private Date data;
	
	public Mensagem( String emissor, String texto){
		this.emissor = emissor;
		this.texto = texto;
		data = new Date();
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getEmissor() {
		return emissor;
	}

	public void setEmissor(String emissor) {
		this.emissor = emissor;
	}

	public String getDataFormatada() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd:MM:yyyy HH:mm");
		return sdf.format(data);
	}
}
