package modelo;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class Sender {
	// Método responsável por fazer o processo de marshalling,
	// transformando um objeto do tipo solicitação em um array de bytes
	public byte[] marshalling(Solicitacao s) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		// Criando um ObjectOutputStream para a serialização do Objeto
		ObjectOutputStream oos;
		try {
			oos = new ObjectOutputStream(baos);
			oos.writeObject(s);
			oos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Transformando o objeto em Bytes para ser enviados via rede
		return baos.toByteArray();
	}


}
