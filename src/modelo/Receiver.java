package modelo;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Observable;

//A classe � um runnable pois ser� uma thread respons�vel por escutar mensagens destinadas ao servidor
//A classe tambem � um observable para a conversa com a interface gr�fica, onde ir� atualizar os componentes de tela

public abstract class Receiver extends Observable implements Runnable {

	// M�todo respons�vel por fazer o processo de unMarshalling,
	// transformando um array de bytes em um objeto do tipo solicita��o
	public Solicitacao unMarshalling(byte[] bytes) {
		Solicitacao s = null;
		
		
		try {
			//Criando um stream de leitura para array de bytes
			ByteArrayInputStream byteStream = new ByteArrayInputStream(bytes);
			ObjectInputStream stream;
			//Criando o ObjectInputStream para a convers�o dos bytes em object 
			stream = new ObjectInputStream(byteStream);
			//Fazendo o casting do objeto lido para o tipo Solicita��o
			s = (Solicitacao) stream.readObject();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return s;

	}

	//M�todos abstratos que devem ser escritos pelas classes concretas
	public abstract String getHost();
	public abstract int getPort();
}
