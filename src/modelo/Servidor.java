package modelo;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;

import excecoes.ErroDeConexaoException;
import excecoes.MensagemException;
import excecoes.SolicitacoNegadaException;

//Classe servidor que representa a pessoa a ser seguida
public class Servidor extends Usuario implements Runnable {

	private ServerSocket ssocket;
	// A cole��o ustilizada para o armazenamento dos seguidores (lista acessada
	// por v�rias threads)
	// foi o Vector, j� que a mesma � thread-safe
	private Vector<Seguidor> seguidores;
	private ServerSender sender;
	private boolean exit = false;

	public Servidor(String nome) {
		super(nome);
		try {

			sender = new ServerSender(this);
			// Instanciando a lista que ir� conter os seguidores (nome ip e
			// porta)
			seguidores = new Vector<Seguidor>();
			// Criando um socket TCP na porta 5050 para escutar as requisi��es
			// dos
			// clientes
			ssocket = new ServerSocket(5050);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void processar() {
		do {
			try {
				// Para cada solicita��o de conex�o � criada uma thread para
				// atend�-la
				// permitindo assim v�rias conex�es simultaneas
				Socket socket = ssocket.accept();
				ServerReceiver receiver = new ServerReceiver(this, socket);
				addObserver(receiver);
				new Thread(receiver).start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} while (notExit());
	}

	public boolean notExit() {
		return !exit;
	}

	public void exit() {
		this.exit = true;
		setChanged();
		notifyObservers(this);
	}
	

	// M�todo que delega ao sender o envio de uma mensagem
	@Override
	public void enviarMensagem(Mensagem m) throws ErroDeConexaoException,
			MensagemException {
		Seguidor s = findSeguidor(m.getEmissor());
		sender.enviarMensagem(m, s);
	}

	public void enviarMensagem(String texto) throws ErroDeConexaoException,
			MensagemException {
		Mensagem m = new Mensagem(getNome(), texto);
		Seguidor seg = new Seguidor(getNome(), ssocket.getLocalPort());
		seg.setIp(ssocket.getInetAddress().getHostAddress());
		setChanged();
		notifyObservers(m);
		LogWriter.writeLog(seg, m);
		sender.enviarMensagem(m, null);
	}

	// M�todo utilizado para a remo��o de um seguidor ao qual foi pedido o
	// Unfollow
	public void removerSeguidor(Seguidor s) throws SolicitacoNegadaException {
		Seguidor localizado = findSeguidor(s.getNome());
		if (localizado != null) {
			seguidores.removeElement(localizado);
			System.out.println("Seguidores :");
			for (Seguidor novo : seguidores) {
				System.out.println(novo.getNome());
			}
		} else {
			throw new SolicitacoNegadaException(
					"Algu�m que n�o � seguidor tentou deixar de seguir");
		}
	}

	// M�todo para a adi��o de um novo seguidor � lista de seguidores
	public void addSeguidor(Seguidor seguidor) throws SolicitacoNegadaException {
		if (findSeguidor(seguidor.getNome()) == null) {
			this.seguidores.add(seguidor);
		} else {
			throw new SolicitacoNegadaException(
					"Negada a solicita��o do seguidor");
		}

	}

	// M�todo para a busca sequancial de um determinado seguidor dado o seu nome
	// (caracterizado por ser chave prim�ria)
	public Seguidor findSeguidor(String nome) {
		for (Seguidor s : seguidores) {
			if (s.getNome().equals(nome)) {
				return s;
			}
		}
		return null;
	}

	// M�todo para retornar a lista com todos os seguidores
	public Vector<Seguidor> getSeguidores() {
		return this.seguidores;
	}

	// m�todo que verifica se um determindao seguidor j� existe na lista
	// impedindo duplicidade de nomes
	public boolean existeSeguidor(Seguidor s) {
		return (findSeguidor(s.getNome()) != null);
	}

	// M�todo que avalia se o cliente est� apto a ser seguidor
	// Respons�vel por enviara resposta da solicita��o
	public void responderSeguir(Seguidor seg) {
		boolean resposta = false;
		try {
			String mensagem;
			if (existeSeguidor(seg)) {
				mensagem = "J� existe um seguidor com o mesmo nome !";
			} else if (seg.getNome().equals(getNome())) {
				mensagem = "O nome do seguidor n�o pode ser o mesmo do seguido !";
			} else {
				mensagem = "Solicita��o Aprovada !";
				addSeguidor(seg);
				resposta = true;
				setChanged();
				notifyObservers(seg);
			}
			sender.enviarRespostaFollow(seg, resposta, new Mensagem(getNome(),
					mensagem));
		} catch (Exception ex) {
			setChanged();
			notifyObservers(ex);
		}
	}

	// M�todo respons�vel por responder ao pedido de um seguidor que quer deixar
	// de seguir
	public void responderDeixarDeSeguir(Seguidor seg) {
		try {
			boolean resposta;
			Mensagem m;
			if (existeSeguidor(seg)) {
				removerSeguidor(seg);
				resposta = true;
				m = new Mensagem(getNome(), "Solicitacao aceita !");
			} else {
				resposta = false;
				m = new Mensagem(getNome(), "O usu�rio n�o � um seguidor !");
			}
			sender.enviarRespostaUnFollow(seg, resposta, m);
			System.out.println("Unfollow nele");
		} catch (Exception e) {
			e.printStackTrace();
			setChanged();
			notifyObservers(e);
		}
	}

	// M�todo que faz a leitura da mensagem recebida
	public void receberMensagem(Solicitacao s) throws MensagemException,
			ErroDeConexaoException {
		Seguidor seg = s.getSeguidor();
		if (existeSeguidor(seg)) {
			enviarMensagem(s.getMensagem());
			LogWriter.writeLog(s.getSeguidor(), s.getMensagem());
			setChanged();
			notifyObservers(s.getMensagem());
		}
	}

	public static void main(String[] args) {
		Servidor s = new Servidor("Julio");
		s.processar();
	}

	@Override
	public void run() {
		this.processar();
	}

}
