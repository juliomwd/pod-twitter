package modelo;
//Classe singleton utilizada como reposit�rio do endere�o do servidor 
//ao qual os clientes ir�o se conectar
//substituindo um arquivo de configura��o
public class ConfiguracaoServidor {
	
	
	private String host = "127.0.0.1";
	private int porta = 5050;
	private static ConfiguracaoServidor instancia = null;
	
	
	public ConfiguracaoServidor(){
		
	}
	
	public static ConfiguracaoServidor getInstance(){
		if(instancia == null){
			instancia = new ConfiguracaoServidor();
		}
		return instancia;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPorta() {
		return porta;
	}

	public void setPorta(int porta) {
		this.porta = porta;
	}
	
	
}
