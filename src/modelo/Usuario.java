package modelo;

import java.util.Observable;

import excecoes.ErroDeConexaoException;
import excecoes.MensagemException;

//Classe abstrata comum ao cliente e ao servidor
public abstract class Usuario extends Observable{
	private String nome;
	public abstract void enviarMensagem(Mensagem m) throws ErroDeConexaoException,MensagemException;
	
	public Usuario(String nome){
		this.nome = nome;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}
