package modelo;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

public abstract class LogWriter {
	public static synchronized void writeLog(Seguidor seg, Mensagem m) {
		try {
			//Abrindo o arquivo onde as mensagens ser�o gravadas
			File arq = new File("log.txt");
			arq.createNewFile();
			FileWriter fileWriter = new FileWriter(arq, true);
			PrintWriter printWriter = new PrintWriter(fileWriter);
			printWriter.println("Emissor: " + m.getEmissor() + " IP/Porta: " + seg.getIp() + ":"
					+ seg.getPorta() + " Data: " + m.getDataFormatada() + " Texto: "
					+ m.getTexto());
			printWriter.flush();
			printWriter.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	
	public static void main(String[] args){
		LogWriter.writeLog(new Seguidor("Julio", 1010),new Mensagem("Julio", "WOLOOWOWOWLOWLWOWO"));
	}
}
