package modelo;

//Enum que define os tipos de solicitação atendidos pelo sistema
public enum TipoSolicitacao {
	MENSAGEM, FOLLOW, UNFOLLOW, RESPONSEFOLLOWTRUE, RESPONSEFOLLOWFALSE , RESPONSEUNFOLLOWTRUE ,RESPONSEUNFOLLOWFALSE;
}
