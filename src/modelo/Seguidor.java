package modelo;

import java.io.Serializable;

//Representa��o do cliente para o lado do servidor
//Implementa serializable pois a mesma ser� enviada via rede
public class Seguidor implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nome;
	private String ip;
	private int porta;
	

	public Seguidor(String nome,int porta) {
		super();
		this.nome = nome;
		this.porta = porta;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPorta() {
		return porta;
	}

	public void setPorta(int porta) {
		this.porta = porta;
	}
}
