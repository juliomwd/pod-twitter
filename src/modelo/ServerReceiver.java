package modelo;

import java.io.IOException;
import java.net.Socket;
import java.util.Observable;
import java.util.Observer;

import excecoes.ErroDeConexaoException;
import excecoes.MensagemException;

//O receiver do lado do servidor, que verifica o tipo de solicita��o enviada
//e realiza a a��o especificada para cada tipo de solicita��o
public class ServerReceiver extends Receiver implements Observer {
	private Servidor servidor;
	private Socket socket;
	private boolean live = true;

	public ServerReceiver(Servidor servidor, Socket socket) {
		this.servidor = servidor;
		this.socket = socket;
	}

	@Override
	public String getHost() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getPort() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void run() {
		try {
			do {
				// Lendo o que foi enviado pelo cliente
				byte[] bytes = new byte[1024];
				socket.getInputStream().read(bytes);

				// Fazendo o processo de unMarshalling com os dados recebidos
				Solicitacao s = unMarshalling(bytes);
				// Colocando o endere�o do ip do cliente que enviouu a
				// solicita��o *
				s.getSeguidor().setIp(socket.getInetAddress().getHostAddress());

				// Encaminhando a solicita��o para ser interpretada
				try {
					processarSolicitacao(s);
				} catch (MensagemException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ErroDeConexaoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} while (notExit());
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean notExit() {
		if (servidor.notExit() == false) {
			return false;
		} else
			return live;
	}

	private void processarSolicitacao(Solicitacao s) throws MensagemException,
			ErroDeConexaoException {
		switch (s.getTipo()) {
		case FOLLOW:
			servidor.responderSeguir(s.getSeguidor());
			break;
		case MENSAGEM:
			servidor.receberMensagem(s);
			break;
		case UNFOLLOW:
			servidor.responderDeixarDeSeguir(s.getSeguidor());
			live = false;
			break;
		default:
			break;

		}

	}

	//M�todo que encerra a conex�o
	private void close() throws ErroDeConexaoException {
		if (socket.isConnected()) {
			try {
				socket.close();
			} catch (IOException e) {
				throw new ErroDeConexaoException("Erro ao fechar a conexao");
			}
		}
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		if (arg1 instanceof Servidor) {
			try {
				this.close();
			} catch (Exception ex) {
				System.out.println(ex.getMessage());
			}
		}
	}

	// * Resolvendo o problema de comunica��o de diferentes n�veis de rede
}
