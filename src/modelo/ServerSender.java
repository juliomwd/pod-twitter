package modelo;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import excecoes.ErroDeConexaoException;

//Classe responsável por enviar solicitações do servidor para os clientes
public class ServerSender extends Sender {
	private DatagramSocket socket;
	private DatagramPacket entrada;
	private Servidor servidor;

	public ServerSender(Servidor s) {
		this.servidor = s;
	}

	//Método responsável por enviar mensagens para os seguidores
	public void enviarMensagem(Mensagem m,Seguidor emissor) throws ErroDeConexaoException {
		try {
			Solicitacao solicitacao ;
			socket = new DatagramSocket();
			byte[] bytes = new byte[1024];
			if(emissor == null){
				solicitacao = new Solicitacao(TipoSolicitacao.MENSAGEM, m);
			}else{
				solicitacao = new Solicitacao(TipoSolicitacao.MENSAGEM, emissor, m);
			}
			bytes = marshalling(solicitacao);
			for (Seguidor s : servidor.getSeguidores()) {
				
				//Enviando as mensagens para todos os seguidores
				InetAddress adress = InetAddress.getByName(s.getIp());
				entrada  = new DatagramPacket(bytes, bytes.length,adress,s.getPorta());
				socket.send(entrada);
			}
		} catch (Exception ex) {
				throw new ErroDeConexaoException("Houve um problema na comunicação com o servidor !");
		}

	}

	// Método responsável por responder a solicitação seguir enviada por um cliente
	public void enviarRespostaFollow(Seguidor seg , boolean resposta,Mensagem mensagem) {
		try {
			TipoSolicitacao tipo;
			if(resposta == true){
				tipo = TipoSolicitacao.RESPONSEFOLLOWTRUE;
			}else{
				tipo = TipoSolicitacao.RESPONSEFOLLOWFALSE;
			}
			socket = new DatagramSocket();
			byte[] bytes = new byte[1024];
			bytes = marshalling(new Solicitacao(tipo, mensagem));
			InetAddress adress = InetAddress.getByName(seg.getIp());
			entrada  = new DatagramPacket(bytes, bytes.length,adress,seg.getPorta());
			socket.send(entrada);			
		} catch (Exception ex) {

		}
	}

	// Método responsável por responder a solicitação do tipo deixar de seguir enviada por um cliente
	public void enviarRespostaUnFollow(Seguidor seg, boolean resposta ,Mensagem m) {
		try {
			TipoSolicitacao tipo;
			if(resposta == true){
				tipo = TipoSolicitacao.RESPONSEUNFOLLOWTRUE;
			}else{
				tipo = TipoSolicitacao.RESPONSEUNFOLLOWFALSE;
			}
			socket = new DatagramSocket();
			byte[] bytes = new byte[1024];
			bytes = marshalling(new Solicitacao(tipo, m));
			InetAddress adress = InetAddress.getByName(seg.getIp());
			entrada  = new DatagramPacket(bytes, bytes.length,adress,seg.getPorta());
			socket.send(entrada);			
		} catch (Exception ex) {

		}
		
	}

}
