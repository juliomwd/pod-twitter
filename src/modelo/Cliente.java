package modelo;

import excecoes.ErroDeConexaoException;
import excecoes.MensagemException;

public class Cliente extends Usuario {
	private ClienteReceiver receiver;
	private ClienteSender sender;
	private boolean seguindo;

	public Cliente(String nome) {
		super(nome);
		seguindo = false;
	}

	public void processar() {
		// Criando uma thread respons�vel por receber as mensagens do servidor
		receiver = new ClienteReceiver(this);
		new Thread(receiver).start();
		// Criando o objeto respons�vel por enviar mensagens ao servidor
		sender = new ClienteSender(this);
	}

	// M�todo respons�vel pelo envio de mensagens
	@Override
	public void enviarMensagem(Mensagem m) throws ErroDeConexaoException,
			MensagemException {
		sender.enviarMensagem(m);
	}

	// M�todo que cria o objeto Mensagem e encaminha-o para o m�todo que invoca
	// o objeto Sender
	public void enviarMensagem(String m) throws ErroDeConexaoException,
			MensagemException {
		Mensagem mess = new Mensagem(getNome(), m);
		enviarMensagem(mess);
	}

	// M�todo respons�vel por enviar uma solicita��o de seguir para o servidor
	public void seguir() throws ErroDeConexaoException {
		sender.enviarSolicitacaoSeguir();
	}

	// M�todo para saber se o cliente j� est� seguindo o servidor
	public boolean estaSeguindo() {
		return seguindo;
	}

	// M�todo usado para confirmar que o servidor recebeu a solicita��o de
	// seguir,
	// atualizando o estado do cliente
	public void confirmarSeguir() {
		seguindo = true;
	}

	// M�todo que envia um pedido de deixar de seguir
	public void deixarDeSeguir() throws ErroDeConexaoException {
		sender.enviarSolicitacaoDeixarDeSeguir();
	}

	// M�todo que ir� alterar o estado do cliente caso o pedido seja recebido
	public void confirmarDeixarDeSeguir() {
		seguindo = false;
	}

	// Retorna o endere�o ip o qual o socket foi criado
	public String getIpReceiver() {
		return receiver.getHost();
	}

	// Retorna a porta a qual o socket de recebimento de mensagens foi criado
	public int getPortReceiver() {
		System.out.println(receiver.getPort());
		return receiver.getPort();
	}

	public Receiver getReceiver() {
		return receiver;
	}
	
	//M�todo que encerra a atividade do cliente
	public void exit() throws ErroDeConexaoException {
		//Caso o cliente esteja seguindo algu�m ele deixa de seguir
		if (this.estaSeguindo()) {
			this.deixarDeSeguir();
		}
	}

}
