package modelo;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

import excecoes.MensagemException;
import excecoes.SolicitacoNegadaException;

//Classe Runnable respons�vel por escutar as mensagens enviadas pelo servidor
public class ClienteReceiver extends Receiver {

	private Cliente cliente;
	private DatagramSocket udpsocket;
	private DatagramPacket entrada;

	public ClienteReceiver(Cliente c) {
		super();
		this.cliente = c;
		try {
			// Criando o socket udp para o recebimento das mensagens
			udpsocket = new DatagramSocket();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		while (notExit()) {
			try {
				// preparando o datagrama que receber� os dados enviados
				entrada = new DatagramPacket(new byte[1024], 1024);
				// recebendo os dados enviados pelo servidor
				udpsocket.receive(entrada);
				// fazendo o unMarshalling dos dados recebidos
				Solicitacao s = unMarshalling(entrada.getData());
				// encaminhando para o m�todo respons�vel por avaliar o tipo de
				// mensagem enviada
				System.out.println(s.getTipo());
				try {
					processarSolicitacao(s);
				} catch (SolicitacoNegadaException e) {
					setChanged();
					notifyObservers(e);
				} catch (MensagemException e) {
					setChanged();
					notifyObservers(e);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private boolean notExit() {
		return true;
	}

	// M�todo que retorn ao ip ao qual o socket foi criado
	@Override
	public String getHost() {
		return udpsocket.getLocalAddress().getHostAddress();
	}

	// M�todo que retorna a porta ao qual o socket foi criado
	@Override
	public int getPort() {
		return udpsocket.getLocalPort();
	}

	// M�todo que processa a solicita��o enviada e encaminha para sua devida
	// opera��o
	public synchronized void processarSolicitacao(Solicitacao s)
			throws SolicitacoNegadaException, MensagemException {

		switch (s.getTipo()) {
		case MENSAGEM:
			Mensagem m = s.getMensagem();
			if (cliente.estaSeguindo()) {
				setChanged();
				if (s.getSeguidor() != null) {
					notifyObservers(m);
				} else {
					notifyObservers(s);
				}
			}
			break;
		case RESPONSEFOLLOWTRUE:
			cliente.confirmarSeguir();
			setChanged();
			notifyObservers(s);
			break;
		case RESPONSEUNFOLLOWTRUE:
			cliente.confirmarDeixarDeSeguir();
			System.out.println("Largeui de seguir");
			setChanged();
			notifyObservers(s);
			break;		
		default:
			throw new SolicitacoNegadaException(s.getMensagem().getTexto());
		}
	}

}
