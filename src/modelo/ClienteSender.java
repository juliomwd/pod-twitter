package modelo;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import excecoes.ErroDeConexaoException;

//Classe respons�vel por enviar solicita��es para o servidor
//Encapsula as mensagem para um tipo de mensagem que o servidor compreenda
public class ClienteSender extends Sender {

	private Socket socket;
	private Seguidor seguidor;
	private Cliente cliente;

	public ClienteSender(Cliente c) {
		this.setCliente(c);
		// Criando o objeto seguidor que representar� o cliente do lado servidor
		// Com as informa��es de nome e porta *
		seguidor = new Seguidor(c.getNome(), c.getPortReceiver());
	}

	// M�todo que prepara uma mensagem para ser enviada ao servidor
	public void enviarMensagem(Mensagem m) {
		try {
			// Criando uma solicita��o que cont�m uma mensagem
			Solicitacao s = new Solicitacao(TipoSolicitacao.MENSAGEM, seguidor,
					m);
			// Fazendo o processo de marshalling
			byte[] bytes = marshalling(s);
			// Enviando a mensagem via socket TCP
			socket.getOutputStream().write(bytes);
		} catch (Exception ex) {
			System.out.println("Deu �guia");
		}

	}

	// M�todo que envia um pedido de seguir do cliente para o servidor
	public void enviarSolicitacaoSeguir() throws ErroDeConexaoException {
		

			// Criando um socket tcp para o envio de mensagens/solicita��es para
			// o servidor
			// obtendo os endere�os ip e porta do servidor via classe singleton
			try {
				socket = new Socket(ConfiguracaoServidor.getInstance().getHost(),
						ConfiguracaoServidor.getInstance().getPorta());
				// Criando uma solicita��o que cont�m uma mensagem
				Solicitacao s = new Solicitacao(TipoSolicitacao.FOLLOW, seguidor);
				// Fazendo o processo de marshalling
				byte[] bytes = marshalling(s);
				// eenviando o pedido via socket TCP
				socket.getOutputStream().write(bytes);
			} catch (UnknownHostException e) {
				throw new ErroDeConexaoException("N�o foi poss�vel encontrar o servidor especificado");
			} catch (IOException e) {
				throw new ErroDeConexaoException("Ocorreu um erro de entrada/sa�da de dados");
			}
			
		
	}

	// M�todo que envia um pedidodo tipo deixar de seguir do cliente para o
	// servidor
	public void enviarSolicitacaoDeixarDeSeguir() throws ErroDeConexaoException {
		try {
			// Criando uma solicita��o que cont�m uma mensagem
			Solicitacao s = new Solicitacao(TipoSolicitacao.UNFOLLOW, seguidor);
			// Fazendo o processo de marshalling
			byte[] bytes = marshalling(s);
			socket.getOutputStream().write(bytes);
		} catch (Exception ex) {
			throw new  ErroDeConexaoException("Houve um erro na conex�o !");
		}
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	// * N�o est� sendo passado o endere�o ip para o objeto Seguidor pois
	// dependendo do n�vel da rede
	// o servidor entenderia o ip de maneira diferente assim se as aplica��es
	// estiverem em uma rede local
	// o endere�o seria diferente se as mesmas estivessem conectadas a internet

}
