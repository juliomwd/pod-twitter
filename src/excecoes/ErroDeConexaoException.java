package excecoes;

public class ErroDeConexaoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ErroDeConexaoException(String message) {
		super(message);
	}

}
