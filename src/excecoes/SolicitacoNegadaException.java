package excecoes;

public class SolicitacoNegadaException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public SolicitacoNegadaException(String message){
		super(message);
	}

}
