package gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import modelo.ConfiguracaoServidor;

public class InicioCliente extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNomeCliente;
	private JTextField txtIP;
	private JTextField txtPorta;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InicioCliente frame = new InicioCliente();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InicioCliente() {
		setTitle("Entrar");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 325, 260);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Nome do Cliente:");
		lblNewLabel.setBounds(24, 21, 104, 23);
		contentPane.add(lblNewLabel);
		
		txtNomeCliente = new JTextField();
		txtNomeCliente.setBounds(127, 22, 141, 20);
		contentPane.add(txtNomeCliente);
		txtNomeCliente.setColumns(10);
		
		JLabel lblConfiguraoDoServidor = new JLabel("Configura\u00E7\u00E3o do Servidor");
		lblConfiguraoDoServidor.setHorizontalAlignment(SwingConstants.CENTER);
		lblConfiguraoDoServidor.setBackground(Color.DARK_GRAY);
		lblConfiguraoDoServidor.setBounds(10, 76, 309, 23);
		contentPane.add(lblConfiguraoDoServidor);
		
		JLabel lblIp = new JLabel("IP:");
		lblIp.setBounds(20, 110, 46, 14);
		contentPane.add(lblIp);
		
		JLabel lblPorta = new JLabel("Porta:");
		lblPorta.setBounds(20, 151, 46, 14);
		contentPane.add(lblPorta);
		
		txtIP = new JTextField();
		txtIP.setBounds(96, 110, 133, 20);
		contentPane.add(txtIP);
		txtIP.setColumns(10);
		
		txtPorta = new JTextField();
		txtPorta.setColumns(10);
		txtPorta.setBounds(96, 148, 58, 20);
		contentPane.add(txtPorta);
		
		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ConfiguracaoServidor.getInstance().setHost(txtIP.getText());
				ConfiguracaoServidor.getInstance().setPorta(Integer.parseInt(txtPorta.getText()));
				TelaCliente cliente = new TelaCliente(txtNomeCliente.getText());
				cliente.setVisible(true);
				setVisible(false);
			}
		});
		btnEntrar.setBounds(201, 203, 89, 23);
		contentPane.add(btnEntrar);
	}
}
