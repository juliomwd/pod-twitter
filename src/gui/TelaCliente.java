package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import modelo.Cliente;
import modelo.Mensagem;
import modelo.Solicitacao;
import modelo.TipoSolicitacao;
import excecoes.ErroDeConexaoException;
import excecoes.MensagemException;

public class TelaCliente extends JFrame implements Observer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPanel panelMensagens;
	private int lastY = 0;
	private LinkedList<PainelMensagem> mensagens;
	private Cliente cliente;
	private JButton btSeguir;
	private JTextArea textArea;

	/**
	 * Create the frame.
	 */
	public TelaCliente(String nomeCliente) {
		setTitle("Timeline Cliente");
		setResizable(false);
		mensagens = new LinkedList<PainelMensagem>();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 696, 610);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		panelMensagens = new JPanel();
		panelMensagens.setBackground(new Color(204, 204, 255));
		// contentPane.add(panelMensagens);
		panelMensagens.setLayout(null);
		panelMensagens.setPreferredSize(new Dimension(400, 500));

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBackground(new Color(204, 204, 255));
		scrollPane.setSize(413, 500);
		scrollPane.setLocation(270, 70);
		scrollPane
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollPane
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBorder(null);
		scrollPane.setPreferredSize(new Dimension(400, 500));
		scrollPane.setViewportView(panelMensagens);
		contentPane.add(scrollPane);

		JLabel lblImagem = new JLabel("");
		lblImagem.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		lblImagem.setIcon(new ImageIcon("img\\user_default.png"));
		lblImagem.setBounds(10, 11, 89, 90);
		contentPane.add(lblImagem);

		JLabel lblNome = new JLabel("New label");
		lblNome.setBounds(121, 36, 101, 14);
		lblNome.setText(nomeCliente);
		contentPane.add(lblNome);

		textArea = new JTextArea();
		textArea.setBounds(10, 125, 240, 72);
		contentPane.add(textArea);

		JButton btSendMessage = new JButton("Enviar Mensagem");
		btSendMessage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (cliente.estaSeguindo()) {
					String texto = textArea.getText();
					try {
						cliente.enviarMensagem(texto);
						textArea.setText("");
					} catch (ErroDeConexaoException e) {
						JOptionPane.showMessageDialog(null, e.getMessage());
					} catch (MensagemException e) {
						JOptionPane.showMessageDialog(null, e.getMessage());
					}
				} else {
					JOptionPane.showMessageDialog(null,
							"Voc� ainda n�o est� seguindo ningu�m");
				}
			}
		});
		btSendMessage.setBorder(UIManager.getBorder("Button.border"));
		btSendMessage.setBackground(Color.WHITE);
		btSendMessage.setBounds(90, 208, 158, 23);
		contentPane.add(btSendMessage);

		btSeguir = new JButton("Seguir");
		btSeguir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					if (!cliente.estaSeguindo()) {
						cliente.seguir();
					} else {
						cliente.deixarDeSeguir();
						mensagens.clear();
						panelMensagens.removeAll();
						refreshMensagens();
					}
				} catch (ErroDeConexaoException ex) {
					JOptionPane.showMessageDialog(null, ex.getMessage());
				}
			}
		});
		btSeguir.setActionCommand("Seguir");
		btSeguir.setBorder(UIManager.getBorder("Button.border"));
		btSeguir.setBackground(Color.WHITE);
		btSeguir.setBounds(109, 70, 144, 23);
		contentPane.add(btSeguir);

		JLabel lblFeedDeMensagens = new JLabel(
				"              Feed de Mensagens");
		lblFeedDeMensagens.setForeground(UIManager
				.getColor("CheckBox.foreground"));
		lblFeedDeMensagens.setBackground(UIManager
				.getColor("Button.disabledShadow"));
		lblFeedDeMensagens.setFont(new Font("Comic Sans MS", Font.PLAIN, 17));
		lblFeedDeMensagens.setBounds(260, 0, 413, 68);
		contentPane.add(lblFeedDeMensagens);

		JButton button = new JButton("Sair");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					cliente.exit();
					System.exit(1);
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, ex.getMessage());
				}
			}
		});
		button.setBorder(UIManager.getBorder("Button.border"));
		button.setBackground(Color.WHITE);
		button.setActionCommand("Seguir");
		button.setBounds(10, 547, 144, 23);
		contentPane.add(button);
		cliente = new Cliente(nomeCliente);
		cliente.processar();
		cliente.getReceiver().addObserver(this);
		cliente.addObserver(this);
	}

	public synchronized void addMensagem(Mensagem m) {
		mensagens.add(0, new PainelMensagem(m, false));
		refreshMensagens();
	}

	private void addMensagemServidor(Mensagem m) {
		mensagens.add(0, new PainelMensagem(m, true));
		refreshMensagens();
	}

	private void refreshMensagens() {
		panelMensagens.repaint();
		lastY = 0;
		for (PainelMensagem pm : mensagens) {
			pm.setBounds(0, nextY(), 400, 100);
			panelMensagens.add(pm);
			pm.setVisible(true);
		}
		panelMensagens.setPreferredSize(new Dimension(400, 100 * mensagens
				.size()));
		panelMensagens.validate();

	}

	public int nextY() {
		int retorno = lastY;
		lastY += 100;
		return retorno;
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		if (arg1 instanceof Mensagem) {
			Mensagem m = (Mensagem) arg1;
			addMensagem(m);
		} else if (arg1 instanceof Solicitacao) {
			Solicitacao s = (Solicitacao) arg1;
			if (s.getTipo() == TipoSolicitacao.MENSAGEM) {
				Mensagem m = ((Solicitacao) arg1).getMensagem();
				addMensagemServidor(m);
			}else if(s.getTipo() == TipoSolicitacao.RESPONSEFOLLOWTRUE){
				btSeguir.setText("Deixar de Seguir");
			}else if(s.getTipo() == TipoSolicitacao.RESPONSEUNFOLLOWTRUE){
				btSeguir.setText("Seguir");
			}
		} else if (arg1 instanceof Exception) {
			JOptionPane
					.showMessageDialog(null, ((Exception) arg1).getMessage());
		}
	}
}
