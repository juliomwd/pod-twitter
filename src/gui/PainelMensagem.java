package gui;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.border.EtchedBorder;

import modelo.Mensagem;

public class PainelMensagem extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JLabel lblEmissor;
	JTextPane txtMensagem;
	JLabel lblData;

	/**
	 * Create the panel.
	 */
	public PainelMensagem(Mensagem m, boolean servidor) {
		if (!servidor) {
			setBackground(new Color(153, 204, 204));
		}else{
			setBackground(new Color(100,200,255));
		}
		setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		setLayout(null);
		construir();
		lblEmissor.setText(m.getEmissor() + " enviou :");
		txtMensagem.setText(m.getTexto());
		lblData.setText(m.getDataFormatada());
		setVisible(true);
	}

	public void construir() {
		lblEmissor = new JLabel();
		lblEmissor.setBounds(10, 11, 192, 14);
		lblEmissor.setFont(new Font("Arial", Font.PLAIN, 11));
		add(lblEmissor);

		txtMensagem = new JTextPane();
		txtMensagem.setFont(new Font("Arial", Font.PLAIN, 12));
		txtMensagem.setBounds(10, 30, 380, 49);
		txtMensagem.setEditable(false);
		add(txtMensagem);

		lblData = new JLabel();
		lblData.setBounds(308, 12, 82, 14);
		lblData.setFont(new Font("Arial", Font.PLAIN, 9));
		add(lblData);
	}
}
