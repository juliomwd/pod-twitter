package gui;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import modelo.Seguidor;

public class SeguidorTableModel extends AbstractTableModel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String[] colunas = new String[] { "Nome", "IP" , "Porta" };
	private List<Seguidor> lista;

	public SeguidorTableModel() {
		lista = new ArrayList<Seguidor>(0);
	}



	@Override
	public int getColumnCount() {

		return colunas.length;
	}

	@Override
	public int getRowCount() {

		return lista.size();
	}

	@Override
	public String getColumnName(int columnIndex) {

		return colunas[columnIndex];
	}

	;

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return String.class;
	}

	;

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Seguidor s = lista.get(rowIndex);

		switch (columnIndex) {

		case 0:
			return s.getNome();
		case 1:
			return s.getIp();
		case 2: 
			return s.getPorta();
		default:
			// Isto n�o deveria acontecer...
			throw new IndexOutOfBoundsException("columnIndex out of bounds");
		}
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	public Seguidor getSeguidor(int indiceLinha) {
		return lista.get(indiceLinha);
	}

	
	public void limpar() {
		lista.clear();
		fireTableDataChanged();
	}

	public boolean isEmpty() {
		return lista.isEmpty();
	}
	
	public void addLista(List<Seguidor> l){
		int tamanhoAntigo = getRowCount();
		this.lista = l;
		fireTableRowsInserted(tamanhoAntigo, getRowCount() - 1);
	}

}
