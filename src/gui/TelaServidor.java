package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import modelo.Mensagem;
import modelo.Seguidor;
import modelo.Servidor;
import excecoes.ErroDeConexaoException;
import excecoes.MensagemException;

public class TelaServidor extends JFrame implements Observer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPanel panelMensagens;
	private int lastY = 0;
	private LinkedList<PainelMensagem> mensagens;
	private Servidor servidor;
	private JTextArea textArea;

	/**
	 * Create the frame.
	 */
	public TelaServidor(String nomeServidor) {
		setTitle("Timeline Servidor");
		setResizable(false);
		mensagens = new LinkedList<PainelMensagem>();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 696, 610);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		panelMensagens = new JPanel();
		panelMensagens.setBackground(new Color(204, 204, 255));
		panelMensagens.setLayout(null);
		panelMensagens.setPreferredSize(new Dimension(400, 500));

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBackground(new Color(204, 204, 255));
		scrollPane.setSize(413, 500);
		scrollPane.setLocation(270, 70);
		scrollPane
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollPane
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBorder(null);
		scrollPane.setPreferredSize(new Dimension(400, 500));
		scrollPane.setViewportView(panelMensagens);
		contentPane.add(scrollPane);

		JLabel lblImagem = new JLabel("");
		lblImagem.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		lblImagem.setIcon(new ImageIcon("img\\user_default.png"));
		lblImagem.setBounds(10, 11, 89, 90);
		contentPane.add(lblImagem);

		JLabel lblNome = new JLabel("New label");
		lblNome.setBounds(121, 36, 101, 14);
		contentPane.add(lblNome);
		lblNome.setText(nomeServidor);
		textArea = new JTextArea();
		textArea.setBounds(10, 125, 240, 72);
		contentPane.add(textArea);

		//Criando Bot�o de Enviar Mensagem e sua respectiva a��o
		JButton btSendMessage = new JButton("Enviar Mensagem");
		btSendMessage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String texto = textArea.getText();
				try {
					servidor.enviarMensagem(texto);
					textArea.setText("");
				} catch (ErroDeConexaoException e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				} catch (MensagemException e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
			}
		});
		btSendMessage.setBorder(UIManager.getBorder("Button.border"));
		btSendMessage.setBackground(Color.WHITE);
		btSendMessage.setBounds(90, 208, 158, 23);
		contentPane.add(btSendMessage);

		//Criando Bot�o sair adicionando a��o 
		JButton btSair = new JButton("Sair");
		btSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				servidor.exit();
				System.exit(1);
			}
		});
		
		btSair.setActionCommand("Seguir");
		btSair.setBorder(UIManager.getBorder("Button.border"));
		btSair.setBackground(Color.WHITE);
		btSair.setBounds(51, 536, 144, 23);
		contentPane.add(btSair);

		JLabel lblFeedDeMensagens = new JLabel(
				"              Feed de Mensagens");
		lblFeedDeMensagens.setForeground(UIManager
				.getColor("CheckBox.foreground"));
		lblFeedDeMensagens.setBackground(UIManager
				.getColor("Button.disabledShadow"));
		lblFeedDeMensagens.setFont(new Font("Comic Sans MS", Font.PLAIN, 17));
		lblFeedDeMensagens.setBounds(260, 0, 413, 68);
		contentPane.add(lblFeedDeMensagens);

		JButton btnVerseguidores = new JButton("Ver Seguidores");
		btnVerseguidores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new ListaSeguidores(servidor.getSeguidores()).setVisible(true);
			}
		});
		btnVerseguidores.setBorder(UIManager.getBorder("Button.border"));
		btnVerseguidores.setBackground(Color.WHITE);
		btnVerseguidores.setActionCommand("Seguir");
		btnVerseguidores.setBounds(106, 89, 144, 23);
		contentPane.add(btnVerseguidores);

		servidor = new Servidor(nomeServidor);
		servidor.addObserver(this);
		new Thread(servidor).start();
	}

	public synchronized void addMensagem(Mensagem m) {
		mensagens.add(0, new PainelMensagem(m, false));
		refreshMensagens();
	}

	public synchronized void addMensagemservidor(Mensagem m) {
		mensagens.add(0, new PainelMensagem(m, true));
		refreshMensagens();
	}

	private void refreshMensagens() {
		panelMensagens.repaint();
		lastY = 0;
		for (PainelMensagem pm : mensagens) {
			pm.setBounds(0, nextY(), 400, 100);
			panelMensagens.add(pm);
			pm.setVisible(true);
		}
		panelMensagens.setPreferredSize(new Dimension(400, 100 * mensagens
				.size()));
		panelMensagens.validate();

	}

	public int nextY() {
		int retorno = lastY;
		lastY += 100;
		return retorno;
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		if (arg1 instanceof Mensagem) {
			Mensagem m = (Mensagem) arg1;
			System.out.println(m.getEmissor());
			System.out.println(servidor.getNome());
			if (m.getEmissor().equals(servidor.getNome())) {
				addMensagemservidor(m);
			} else
				addMensagem(m);
		} else if (arg1 instanceof Exception) {
			System.out.println(((Exception) arg1).getMessage());
		} else if(arg1 instanceof Seguidor){
			System.out.println("Adicionado novo seguidor : " + ((Seguidor) arg1).getNome());
		}
	}

	
}
