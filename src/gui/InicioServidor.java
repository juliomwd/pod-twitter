package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class InicioServidor extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNomePessoa;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public InicioServidor() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 327, 230);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNomeDaPessoa = new JLabel("Nome da Pessoa a ser seguida");
		lblNomeDaPessoa.setHorizontalAlignment(SwingConstants.CENTER);
		lblNomeDaPessoa.setBounds(0, 11, 321, 14);
		contentPane.add(lblNomeDaPessoa);
		
		txtNomePessoa = new JTextField();
		txtNomePessoa.setHorizontalAlignment(SwingConstants.CENTER);
		txtNomePessoa.setBounds(49, 36, 234, 20);
		contentPane.add(txtNomePessoa);
		txtNomePessoa.setColumns(10);
		
		JButton btnIniciar = new JButton("Iniciar");
		btnIniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaServidor tela = new TelaServidor(txtNomePessoa.getText());
				tela.setVisible(true);
				setVisible(false);
			}
		});
		btnIniciar.setBounds(114, 116, 89, 23);
		contentPane.add(btnIniciar);
	}
	
	public static void main(String[] args) {
		new InicioServidor().setVisible(true);
	}

}
