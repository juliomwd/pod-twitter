package gui;

import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import modelo.Seguidor;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;

public class ListaSeguidores extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	private SeguidorTableModel modelo;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public ListaSeguidores(List<Seguidor> seguidores) {
		setTitle("Lista de Seguidores");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 323, 231);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		modelo = new SeguidorTableModel();
		table = new JTable();
		table.setModel(modelo);
		table.setBounds(26, 53, 262, 119);
		modelo.addLista(seguidores);
		contentPane.add(table);
		
		JLabel lblNewLabel = new JLabel("Lista De Seguidores");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(0, 11, 317, 31);
		contentPane.add(lblNewLabel);
	}
}
